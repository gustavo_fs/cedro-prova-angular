import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { RecebidasComponent } from './recebidas/recebidas.component';
import { TermosComponent } from './termos/termos.component';
import { PerfilComponent } from './perfil/perfil.component';
import { SucessoComponent } from './sucesso/sucesso.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  { path: '', redirectTo: 'recebidas', pathMatch: 'full' },
  { path: 'recebidas', component: RecebidasComponent },
  { path: 'termos', component: TermosComponent },
  { path: 'perfil', component: PerfilComponent },
  { path: 'sucesso', component: SucessoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]
})

export class AppRoutingModule { 

  
}
