import { Injectable } from '@angular/core';

import { Perguntas } from './perguntas';
import { colection } from './colection';
import { opcoes  } from './opcoes';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor() { }

  getPerguntas(): Perguntas[] {
    return colection;
  }

  getOpcoes(): string[] {
    return opcoes.opcoes;
  }
}
