import { Perguntas } from './perguntas';

export const opcoes: Perguntas = 
  { pergunta: 'General',
    opcoes: [
      'Nunca operei',
      'Opero pouco e não tenho familiaridade',
      'Opero eventualmente e conheço os riscos associados',
      'Opero frequentemente e conheço os riscos associados'
    ]};
