import { Perguntas } from './perguntas';

export const colection: Perguntas[] = [
    { pergunta: '1 - Qual seu comportamento em relação aos seus investimentos?', 
      opcoes: [
        'Preservar meu dinheiro sem correr riscos',
        'Ganhar mais dinheiro, assumindo riscos moderados',
        'Ganhar mais dinheiro, assumindo riscos agressivos']},

    { pergunta: '2 - Por quanto tempo você deseja manter os seus investimentos?', 
      opcoes: [
        'Até 1 ano',
        'De 1 a 3 anos',
        'De 3 a 10 anos']},

    { pergunta: '3 - Quantos % desses investimentos você pode precisar em um ano?', 
      opcoes: [
        'Acima de 75%',
        'De 26% a 74%',
        'Até 25%']}
];