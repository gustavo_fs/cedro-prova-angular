import { Component, OnInit } from '@angular/core';
import { QuestionsService } from './questions.service';
import { Perguntas } from './perguntas';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  perguntas: Perguntas[];

  constructor(public questionsService: QuestionsService) { }

  ngOnInit() {
    this.perguntas = this.questionsService.getPerguntas();
  }

}
