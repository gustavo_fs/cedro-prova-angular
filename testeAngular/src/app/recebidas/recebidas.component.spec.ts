import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecebidasComponent } from './recebidas.component';

describe('RecebidasComponent', () => {
  let component: RecebidasComponent;
  let fixture: ComponentFixture<RecebidasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecebidasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecebidasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
