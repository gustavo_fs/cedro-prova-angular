import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recebidas',
  templateUrl: './recebidas.component.html',
  styleUrls: ['./recebidas.component.css']
})
export class RecebidasComponent implements OnInit {

  titulo: string = 'Informações Recebidas!';

  constructor() { }

  ngOnInit() {
  }

}
