import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RecebidasComponent } from './recebidas/recebidas.component';
import { TermosComponent } from './termos/termos.component';
import { PerfilComponent } from './perfil/perfil.component';
import { SucessoComponent } from './sucesso/sucesso.component';
import { AppRoutingModule } from './/app-routing.module';
import { QuestionsService } from './perfil/questions.service';

@NgModule({
  declarations: [
    AppComponent,
    RecebidasComponent,
    TermosComponent,
    PerfilComponent,
    SucessoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [QuestionsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
